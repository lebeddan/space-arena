using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpLoot : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        PlayerManager manager = collision.GetComponent<PlayerManager>();

        if(manager)
        {
            bool pickedUp = manager.PickUpItem(gameObject);
            if (pickedUp)
            {
                Destroy(gameObject);
            }
        }
    }    
}
