using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpController : MonoBehaviour
{
    public Gun gunScript;
    public Rigidbody rb;
    public BoxCollider coll;
    public Transform player, gunContainer, fpsCam;
    public Animator gunAnimator;

    public float pickUpRange;
    public float dropForwardForce, dropUpwardForce;

    public bool equipped;

    private void Start()
    {
        //Setup
        if (!equipped)
        {
            gunScript.enabled = false;
            rb.isKinematic = false;
            coll.isTrigger = false;
            gunAnimator.enabled = false;
        }
        if (equipped)
        {
            gunScript.enabled = true;
            rb.isKinematic = true;
            coll.isTrigger = true;
            gunAnimator.enabled = true;
            //slotFull = true;
        }
    }

    private void Update()
    {
        //Check if player is in range and "E" is pressed
        Vector3 distanceToPlayer = player.position - transform.position;
        if (!equipped && distanceToPlayer.magnitude <= pickUpRange && Input.GetKeyDown(KeyCode.E) /*&& !slotFull*/) PickUp();

        //Drop if equipped and "Q" is pressed
        if (equipped && Input.GetKeyDown(KeyCode.Q)) Drop();
    }

    public void PickUp()
    {
        equipped = true;
        //slotFull = true;

        //Make weapon a child of the camera and move it to default position
        transform.SetParent(gunContainer);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        //transform.localScale = Vector3.one;                           // seem to be unneeded, scale is copied as it is


        //Make Rigidbody kinematic and BoxCollider a trigger
        rb.isKinematic = true;
        coll.isTrigger = true;
        gunAnimator.enabled = true;

        //Enable script
        gunScript.enabled = true;
        this.gameObject.layer = 11;                                    // set layer to Weapons (11)
        /////////////////////////////////////////////// TEST //////////////////////////////////////////////
        //gunContainer.GetComponent<WeaponSwitching>().addNew(transform);
        ///////////////////////////////////////////////////////////////////////////////////////////////////
    }

    public void Drop()
    {
        equipped = false;
        //slotFull = false;
        //Disable script
        gunScript.enabled = false;
        gunAnimator.enabled = false;

        //Set parent to null
        transform.SetParent(null);

        //Make Rigidbody not kinematic and BoxCollider normal
        rb.isKinematic = false;
        coll.isTrigger = false;

        //Gun carries momentum of player
        //rb.velocity = player.GetComponent<Rigidbody>().velocity;                 player has no rigidbody

        //AddForce
        rb.AddForce(fpsCam.forward * dropForwardForce, ForceMode.Impulse);
        rb.AddForce(fpsCam.up * dropUpwardForce, ForceMode.Impulse);
        //Add random rotation
        float random = Random.Range(-1f, 1f);
        rb.AddTorque(new Vector3(random, random, random) * 10);
        
        this.gameObject.layer = 0;                                         // set layer to Default (0)

    }
}
