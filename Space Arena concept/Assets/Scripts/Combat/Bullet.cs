using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    /// 
    /// The goal of this class is to show bullet trail after shooting
    /// 
   
    // Make bullet trail appear and start fading
    // parameters: start - is where trail starts, end - where it ends 
    public void Execute(Vector3 start, Vector3 end)
    {
        GameObject trail = gameObject;
        transform.position = start;
        LineRenderer lr = trail.GetComponent<LineRenderer>();
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        StartCoroutine(TrailFade(trail));
    }


    // bullet trail fading for a few frames
    // parameter: trail - trail line
    IEnumerator TrailFade(GameObject trail)
    {
        for (int i = 0; i < 2; i++)
        {
            yield return new WaitForEndOfFrame();
        }
        Destroy(trail);
    }
}
