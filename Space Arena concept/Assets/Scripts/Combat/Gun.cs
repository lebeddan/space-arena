using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{

    public float damage = 10f;
    public float range = 100f;
    public float coolDownTime = 0.2f;

    public float impactForce = 30f;

    public Transform spawnPoint;

    public AudioSource ShootAudioSource;
    public AudioClip shootAudio;
    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public GameObject impactEffect;
    public Bullet bulletPrefab;

    Animator gun_animator;

    private bool can_shoot = true;

    PlayerMovement player;

    IEnumerator coroutine;

    void Start()
    {
        gun_animator = GetComponent<Animator>();
        gun_animator.SetFloat("RecoilTime", 1/coolDownTime);

        coroutine = waiting();
        ShootAudioSource = GetComponent<AudioSource>();
        player = gameObject.GetComponentInParent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            gun_animator.SetBool("Walk", false);
        }
        else
        {
            gun_animator.SetBool("Walk", true);
        }

        if (player.isGrounded)
        {
            gun_animator.SetBool("Falling", false);
            //Debug.Log("Grounded");
        }
        else
        {
            gun_animator.SetBool("Falling", true);
            //Debug.Log("InAir");
        }


        if (Input.GetButtonDown("Fire1") && !gun_animator.GetCurrentAnimatorStateInfo(0).IsName("out_of_purse"))
        {
            Shoot();
            ShootAudioSource.PlayOneShot(shootAudio);
        }
    }

    public void Shoot()
    {
        if (can_shoot)
        {
            muzzleFlash.Play();
            gun_animator.SetTrigger("Shoot");

            RaycastHit hit;
            Ray ray = new Ray();

            ray.direction = fpsCam.transform.forward;
            ray.origin = fpsCam.transform.position;
            
            
            if (Physics.Raycast(ray, out hit, range, ~LayerMask.GetMask("Player")))
            {
              
                AnimateProjectile(spawnPoint.position, hit.point);
                Debug.Log(hit.transform.name);
                
                IDamageable target = hit.transform.GetComponent<IDamageable>();

                if (target != null)
                {
                    target.TakeDamage(damage);
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * impactForce);
                }

                
                GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal)); // object must have rigidbody
                Destroy(impactGO, 2f);
            }
            else{
                AnimateProjectile(spawnPoint.position, ray.GetPoint(range));
            }


            ///////////////////////////////////// SHOOTING ANIMATION CONTROL/////////////////////////////////////////////////////////////////

            StopCoroutine(coroutine);
            gun_animator.SetBool("Steady", true);
            coroutine = waiting();
            StartCoroutine(coroutine);
            //////////////////////////////////////////COOL DOWN//////////////////////////////////////////////////////////////
            
            StartCoroutine("coolDown");
        }

    }

    void AnimateProjectile(Vector3 start, Vector3 end)
    {
        Bullet bull = GameObject.Instantiate<Bullet>(bulletPrefab, start, Quaternion.LookRotation(end));
        bull.Execute(start , end);
    }

    // Waits fixed time before playing movements animation, in order to make shooting animation more smooth 
    IEnumerator waiting()
    { 
        float t = 0;
        while(t < coolDownTime * 3)
        {
            yield return new WaitForFixedUpdate();
            //Debug.Log("coroutine_running");
            t += Time.deltaTime;
        }
        gun_animator.SetBool("Steady", false);

    }

    // doesnt let you shoot too often
    IEnumerator coolDown()
    {
        can_shoot = false;
        yield return new WaitForSeconds(coolDownTime);
        can_shoot = true;
    }
}
