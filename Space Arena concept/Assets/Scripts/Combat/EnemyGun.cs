using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
/*
 * The script responsible for the parameters of the enemy gun, damage to the player and bullet animation
 */
public class EnemyGun : MonoBehaviour
{
    [Header("Main parametrs")]
    public float shootSpeed;
    public float damage;
    public float range;
    private float coolDownTimer;
    
    [Header("Reference")] 
    public Transform[] bulletStartPos;
    public Laser laserPrefab;
    public HealthController hpController;
    public ArmorController armorController;
    public AudioSource enemyGunAudioSource;
    public AudioClip enemyGunAudio;

    public float secondsOfWaiting = 1f;

    public void Start()
    {
        enemyGunAudioSource = GetComponent<AudioSource>();
    }
    
    public void Shoot(Vector3 target)
    {
        //Cooldown
        coolDownTimer -= Time.deltaTime;
        if (coolDownTimer > 0) return;
        coolDownTimer = shootSpeed;
        
        RaycastHit hit;
        Ray ray = new Ray();
        ray.direction = transform.forward;
        ray.origin = transform.position;
        
        
        // shoot the hit in range
        if (Physics.Raycast(ray, out hit, range,~LayerMask.GetMask("Enemy")))
        {
            enemyGunAudioSource.PlayOneShot(enemyGunAudio);
            AnimateShoot(target);
            
            if (hit.transform.CompareTag("Player"))
            {
                if (armorController.haveArmor)
                {
                    armorController.currentPlayerArmor -= damage;
                    if (armorController.currentPlayerArmor <= 0)
                    {
                        armorController.haveArmor = false;
                    }
                    else
                        armorController.TakeDamage();
                }
                else
                {
                    if (hpController.currentPlayerHP > 0)
                    {
                        hpController.currentPlayerHP -= damage;
                        if (hpController.currentPlayerHP <= 0)
                        {
                            //When player dies, he spawn on respawn point
                            //hpController.currentPlayerHP = hpController.maxPlayerHP;
                            //hpController.playerMn.ResetPlayer();
                            StartCoroutine(ReadingPause());
                        }
                        else
                            hpController.TakeDamage();
                    }
                }
            }
        }else AnimateShoot(target);
    }

    private void AnimateShoot(Vector3 target)
    {
        //Set direction on target
        Vector3 laserDirection = new Vector3(0, 0, 0);
        foreach (Transform bulletStart in bulletStartPos)
        { 
            laserDirection = target - bulletStart.position;
            laserDirection.Normalize();
        }
     
        //Create bullet shot
        foreach (Transform bulletStart in bulletStartPos)
        {
            Laser bullet = GameObject.Instantiate(
                laserPrefab,
                bulletStart.position + laserDirection * 0.01f,
                Quaternion.LookRotation(laserDirection)
            );
            bullet.Execute(bulletStart.position, target);
        }
    }

    private IEnumerator ReadingPause()
    {

        yield return new WaitForSeconds(secondsOfWaiting);
        hpController.currentPlayerHP = hpController.maxPlayerHP;
        hpController.playerMn.ResetPlayer();

    }

}
