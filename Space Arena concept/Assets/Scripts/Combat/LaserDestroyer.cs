using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserDestroyer : MonoBehaviour
{

    public float damage = 10f;
    public float range = 100f;
    public float impactForce = 0f;


    public int typeOfAmmo = 3;

    private int magazine;
    public int magazineCapacity = 1;

    
    private InputField display;


    public Transform spawnPoint;
    public Camera fpsCam;
    
    public GameObject impactEffect;

    public Bullet bulletPrefab;

    Animator gun_animator;

    private bool can_shoot = true;
    private bool reloading = false;
    private bool shootingInProcess = false;

    PlayerMovement player;
    Inventory playerInventory;

    GameObject enemy = null;

    IEnumerator coroutine;

    void Start()
    {
        gun_animator = GetComponent<Animator>();

        coroutine = Shooting();

        playerInventory = gameObject.GetComponentInParent<Inventory>();
        fpsCam = playerInventory.fpsCam.GetComponent<Camera>();

        if (playerInventory.ammunition[typeOfAmmo] >= magazineCapacity)
        {
            playerInventory.ammunition[typeOfAmmo] -= magazineCapacity;
            magazine = magazineCapacity;
        }
        else
        {
            magazine = playerInventory.ammunition[typeOfAmmo];
            playerInventory.ammunition[typeOfAmmo] = 0;
        }

        player = gameObject.GetComponentInParent<PlayerMovement>();

        display = transform.GetComponentInChildren<InputField>();
        display.text = (magazine + " / " + playerInventory.ammunition[typeOfAmmo]);
        gun_animator.SetBool("Steady", true);
    }

    private void OnEnable()
    {
        reloading = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
            return;

        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            gun_animator.SetBool("Walk", false);
        }
        else
        {
            gun_animator.SetBool("Walk", true);
        }

        if (player.isGrounded)
        {
            gun_animator.SetBool("Falling", false);
            //Debug.Log("Grounded");
        }
        else
        {
            gun_animator.SetBool("Falling", true);
            //Debug.Log("InAir");
        }


        if (Input.GetButtonDown("Fire1") && !gun_animator.GetCurrentAnimatorStateInfo(0).IsName("out_of_purse"))
        {
            Shoot();
        }

        display.text = (magazine + " / " + playerInventory.ammunition[typeOfAmmo]);
    }

    void Shoot()
    {
        if (shootingInProcess)
            return;

        if (reloading)
            return;

        if (magazine <= 0)
        {
            can_shoot = false;
            if (playerInventory.ammunition[typeOfAmmo] > 0)
            {
                gun_animator.SetTrigger("Reloading");
                reloading = true;
                StartCoroutine(reload());
                return;
            }
        }

        if (can_shoot)
        {
            magazine--;

            StartCoroutine(coroutine);
            coroutine = Shooting();

        }

    }

    void AnimateProjectile(Vector3 start, Vector3 end)
    {
        Bullet bull = GameObject.Instantiate<Bullet>(bulletPrefab, start, Quaternion.LookRotation(end));
        bull.Execute(start, end);
    }

    
    IEnumerator reload()
    {
        Debug.Log("Reloading...");

        while (!gun_animator.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            yield return new WaitForFixedUpdate();
        }
        while (gun_animator.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            yield return new WaitForFixedUpdate();
        }

        if (playerInventory.ammunition[typeOfAmmo] >= magazineCapacity)
        {
            playerInventory.ammunition[typeOfAmmo] -= magazineCapacity;
            magazine = magazineCapacity;
        }
        else
        {
            magazine = playerInventory.ammunition[typeOfAmmo];
            playerInventory.ammunition[typeOfAmmo] = 0; ;
        }
        reloading = false;
        can_shoot = true;
        Debug.Log("Finished Reloading.");
    }

   
    IEnumerator Shooting()
    {
        shootingInProcess = true;
        float time = 0f;
        while (time < 5)
        {
            yield return new WaitForEndOfFrame();
            time += Time.deltaTime;

            RaycastHit hit;
            Ray ray = new Ray();

            ray.direction = fpsCam.transform.forward;
            ray.origin = fpsCam.transform.position;

            if (Physics.Raycast(ray, out hit, range, ~LayerMask.GetMask("Player")))
            {
                IDamageable target = hit.transform.GetComponent<IDamageable>();
                
                if (target != null)
                {
                    if (enemy != hit.transform.gameObject)
                    {
                        target.TakeDamage(damage);
                        enemy = hit.transform.gameObject;
                        StartCoroutine(CountDown(hit.transform.gameObject));
                    }
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * impactForce);
                }


                AnimateProjectile(spawnPoint.position, hit.point);
                GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal)); // object must have rigidbody
                Destroy(impactGO, 2f);
            }
            else
            {
                AnimateProjectile(spawnPoint.position, ray.GetPoint(range));
            }
        }
        shootingInProcess = false;

    }

    IEnumerator CountDown(GameObject e)
    {
        yield return new WaitForSeconds(1f);
        if(e == enemy)
            enemy = null;
    } 
}
