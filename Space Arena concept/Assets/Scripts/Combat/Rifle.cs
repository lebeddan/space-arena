using System.Collections;

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Rifle shooting and animating program
/// </summary>
public class Rifle : MonoBehaviour
{
    private float damage = 10f; 
    private float range = 100f;
    private int typeOfAmmo = 0;
    private int magazine;
    private int magazineCapacity = 10;
    private bool reloading = false;
    
    private InputField display;

    public float coolDownTime = 0.2f;   // how long rifle cannot shoot after previous shot
    public float impactForce = 30f;     // force upplied to rigid bodies shot at
    public Transform spawnPoint;        // point where trail starts
    public Camera fpsCam;               // player camera
    public ParticleSystem muzzleFlash;  // weapon muzzle flash
    public GameObject impactEffect;     // effect on impact when shooting
    public float idleStateTime = 5f;    // time after shot for which gun animation is minimized

    public Bullet bulletPrefab;     // bullet trail prefab

    private Animator gun_animator;          

    private bool can_shoot = true; 

    private PlayerMovement player;
    private Inventory playerInventory;

    IEnumerator coroutine;

    public AudioSource sound;   // sound component of the weapon game object
    public AudioClip shootSound;    // sound to be played after shot


    void Start()
    {
        Info inf = transform.GetComponent<Info>();
        if (inf)
        {
            this.damage = inf.damage;
            this.range = inf.range;
            this.magazineCapacity = inf.capacity;
            this.typeOfAmmo = inf.ammo;
        }

        gun_animator = GetComponent<Animator>();
        gun_animator.SetFloat("RecoilTime", 1 / coolDownTime);

        coroutine = waiting();

        playerInventory = gameObject.GetComponentInParent<Inventory>();
        fpsCam = playerInventory.fpsCam.GetComponent<Camera>();

        if (playerInventory.ammunition[typeOfAmmo] >= magazineCapacity)
        {
            playerInventory.ammunition[typeOfAmmo] -= magazineCapacity;
            magazine = magazineCapacity;
        }
        else
        {
            magazine = playerInventory.ammunition[typeOfAmmo];
            playerInventory.ammunition[typeOfAmmo] = 0; ;
        }

        player = gameObject.GetComponentInParent<PlayerMovement>();

        display = transform.GetComponentInChildren<InputField>();
        display.text = (magazine + " / " + playerInventory.ammunition[typeOfAmmo]);
    }

    // so that possibly interrupted coroutines do not prevent gun from shooting
    private void OnEnable()
    {
        reloading = false;
        can_shoot = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
            return;

        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            gun_animator.SetBool("Walk", false);
        }
        else
        {
            gun_animator.SetBool("Walk", true);
        }

        if (player.isGrounded)
        {
            gun_animator.SetBool("Falling", false);
            //Debug.Log("Grounded");
        }
        else
        {
            gun_animator.SetBool("Falling", true);
            //Debug.Log("InAir");
        }


        if (Input.GetButton("Fire1") && !gun_animator.GetCurrentAnimatorStateInfo(0).IsName("out_of_purse")&& !gun_animator.GetCurrentAnimatorStateInfo(0).IsName("Recoil"))
        {
            Shoot();
        }

        display.text = (magazine + " / " + playerInventory.ammunition[typeOfAmmo]);
    }


    // Shooting function
    void Shoot()
    {
        if (reloading)
            return;

        if (magazine <= 0)
        {
            can_shoot = false;
            if (playerInventory.ammunition[typeOfAmmo] > 0)
            {
                gun_animator.SetTrigger("Reloading");
                reloading = true;
                StartCoroutine(reload());
                return;
            }
        }

        if (can_shoot)
        {
            sound.PlayOneShot(shootSound);
            magazine--;
            muzzleFlash.Play();
            gun_animator.SetTrigger("Shoot");

            RaycastHit hit;
            Ray ray = new Ray();

            ray.direction = fpsCam.transform.forward;
            ray.origin = fpsCam.transform.position;
            if (Physics.Raycast(ray, out hit, range, ~LayerMask.GetMask("Player")))
            {

                AnimateProjectile(spawnPoint.position, hit.point);


                //Debug.Log(hit.transform.name);

                IDamageable target = hit.transform.GetComponent<IDamageable>();

                if (target != null)
                {
                    target.TakeDamage(damage);
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * impactForce);
                }


                GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal)); // object must have rigidbody
                Destroy(impactGO, 2f);
            }
            else
            {
                AnimateProjectile(spawnPoint.position, ray.GetPoint(range));
            }


            ///////////////////////////////////// SHOOTING ANIMATION CONTROL/////////////////////////////////////////////////////////////////

            StopCoroutine(coroutine);
            gun_animator.SetBool("Steady", true);
            coroutine = waiting();
            StartCoroutine(coroutine);
            //////////////////////////////////////////COOL DOWN//////////////////////////////////////////////////////////////

            StartCoroutine("coolDown");
        }

    }

    // make projectile animation
    void AnimateProjectile(Vector3 start, Vector3 end)
    {
        Bullet bull = GameObject.Instantiate<Bullet>(bulletPrefab, start, Quaternion.LookRotation(end));
        bull.Execute(start, end);
    }

    // Waits fixed time before playing movements animation, in order to make shooting animation more smooth 
    IEnumerator waiting()
    {
        float t = 0;
        while (t < idleStateTime)
        {
            yield return new WaitForFixedUpdate();
            // Debug.Log("coroutine_running");
            t += Time.deltaTime;
        }
        gun_animator.SetBool("Steady", false);

    }

    // reloading process
    IEnumerator reload()
    {
        Debug.Log("Reloading...");

        while (!gun_animator.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            yield return new WaitForFixedUpdate();
        }
        while (gun_animator.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            yield return new WaitForFixedUpdate();
        }

        if (playerInventory.ammunition[typeOfAmmo] >= magazineCapacity)
        {
            playerInventory.ammunition[typeOfAmmo] -= magazineCapacity;
            magazine = magazineCapacity;
        }
        else
        {
            magazine = playerInventory.ammunition[typeOfAmmo];
            playerInventory.ammunition[typeOfAmmo] = 0; ;
        }
        reloading = false;
        can_shoot = true;
        Debug.Log("Finished Reloading.");
    }

    // doesnt let you shoot too often
    IEnumerator coolDown()
    {
        can_shoot = false;
        yield return new WaitForSeconds(coolDownTime);
        can_shoot = true;
    }
    
}
