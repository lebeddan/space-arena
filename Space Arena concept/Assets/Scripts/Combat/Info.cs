using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Info : MonoBehaviour
{
    ///
    /// This is a secondary weapon class where weapon information is entered for main weapon class ro use and kept so that other programms may access it if needed
    ///
   
    public string model;    // weapon model
    public int ammo;        // type of ammo
    public int capacity;    // weapon magazine capacity
    public float damage;    // damage per piece of projectile
    public float range;     // at what range does projectile damage enemies
    public Sprite icon;     // weapon icon
}
