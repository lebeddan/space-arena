using System.Collections;
using UnityEngine;

/// <summary>
///     This class animates the movement of laser projectile
/// </summary>
public class Laser : MonoBehaviour
{


    /// <summary>
    /// Start laser movement
    /// </summary>
    /// <param name="start"></param> Start of the  laser movement
    /// <param name="end"></param>  End of the  laser movement
    public void Execute(Vector3 start, Vector3 end)
    {
        GameObject laser = gameObject;//Instantiate(gameObject);
        transform.position = start;
        StartCoroutine(Move(laser, end, start));
    }


    /// <summary>
    /// Laser movement process
    /// </summary>
    /// <param name="laser"></param>    laser prefab
    /// <param name="end"></param>      End of the  laser movement
    /// <param name="start"></param>    Start of the  laser movement
    /// <returns></returns>
    IEnumerator Move(GameObject laser, Vector3 end, Vector3 start )
    {
        Vector3 direction = (end - start).normalized;
        float current = 0.0f;
        float duration = 5f;
        while (current < duration)
        {
            yield return new WaitForFixedUpdate();
            laser.transform.position = laser.transform.position + direction * 10;
            current += Time.fixedDeltaTime;
            if (Vector3.Distance(end, start) <= Vector3.Distance(laser.transform.position, start))
            {
                break;
            }
        }
        Destroy(laser);
    }

}
