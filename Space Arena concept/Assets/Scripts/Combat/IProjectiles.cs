
using UnityEngine;

public interface IProjectiles
{
    public void Execute(Vector3 start, Vector3 end);

    
}
