using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Checks area around weapon in hand collides with other objects
/// </summary>
public class WeaponBox : MonoBehaviour
{
    public bool collison = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Weapon") && other.gameObject.layer != LayerMask.NameToLayer("Player"))
        {
           // print("Entered collision" + other.name);
            collison = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Weapon") && other.gameObject.layer != LayerMask.NameToLayer("Player"))
        {
            //print("Exited collision" + other.name);
            collison = false;
        }
    }
}
