using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunState : MonoBehaviour
{
    /// 
    /// This class switches wepon Game Object components when it is dropped or picked up  
    /// 

    public Behaviour script;    // weapon shooting script component
    public Animator animator;   // weapon animator component
    public Rigidbody rb;        // weapon rigidbody compnent


    // switch when weapon is dropped
    public void Off()
    {
        script.enabled = false;

        animator.enabled = false;

        rb.isKinematic = false;

        

    }


    // switch when player picks weapon up
    public void On()
    {
        script.enabled = true;

        animator.enabled = true;

        rb.isKinematic = true;

        

        
    }
}
