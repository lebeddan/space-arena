using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class with which player picks up wepons
/// </summary>
public class pickUp : MonoBehaviour
{
    
    public float range = 5f;    // how far weapon is detected

    public Camera fpsCam;       // player camera

    private WeaponSwitching switcher;  

    private Behaviour lookingAt;       

    // Start is called before the first frame update
    void Start()
    {
        switcher = transform.GetComponent<WeaponSwitching>();
    }


    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray();
        ray.origin = fpsCam.transform.position;
        ray.direction = fpsCam.transform.forward;
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, range, LayerMask.GetMask("PickableItem")))
        {
            if (Input.GetKeyDown(KeyCode.E) && hit.transform.tag == "Gun")
            {
                switcher.addNew(hit.transform);
            }



            Behaviour b = hit.collider.GetComponent<Outline>();

            if (lookingAt != b && lookingAt != null)
            {
                lookingAt.enabled = false;
            }
            
            if(b != null)
            {
                lookingAt = b;
                b.enabled = true;
            }

            
        }
        else if(lookingAt != null)
        {
            lookingAt.enabled = false;
        }
        
    }
}