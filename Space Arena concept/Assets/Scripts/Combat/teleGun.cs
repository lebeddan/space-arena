using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class teleGun : MonoBehaviour
{
    public GameObject prefab;   // teleport prefab
    public Animator levelLoader;    // animator of the level loading
    public float radius = 2f;       // radius of check for collision with instantiated teleport after shooting
    public Camera fpsCam;           // player camera
    public float range = 10f;       // how far may teleport be spawned 
    public GameObject fuelTank;     // fuel tank of the teleportation gun model
    public Text start_text, end_text;   // start and end text of level loading animation
    public float coolDownTime = 0.2f;   // how long rifle cannot shoot after previous shot


    private int typeOfAmmo;
    private GameObject teleport;
    private int magazine;
    private int magazineCapacity = 10;
    private bool reloading = false;
    private Animator gun_animator;
    private bool can_shoot = true;

    private PlayerMovement player;
    private Inventory playerInventory;

    IEnumerator coroutine;

    void Start()
    {
        if(magazine == 0)
        {
            fuelTank.SetActive(false);
        }

        Info inf = transform.GetComponent<Info>();
        if (inf)
        {
            this.magazineCapacity = inf.capacity;
            this.typeOfAmmo = inf.ammo;
        }

        gun_animator = GetComponent<Animator>();
        gun_animator.SetFloat("RecoilTime", 1 / coolDownTime);

        coroutine = waiting();

        playerInventory = gameObject.GetComponentInParent<Inventory>();
        fpsCam = playerInventory.fpsCam.GetComponent<Camera>();

       

        player = gameObject.GetComponentInParent<PlayerMovement>();

    }

    // so that possibly interrupted coroutines do not prevent gun from shooting
    private void OnEnable()
    {
        reloading = false;
        can_shoot = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
            return;

        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
        {
            gun_animator.SetBool("Walk", false);
        }
        else
        {
            gun_animator.SetBool("Walk", true);
        }

        if (player.isGrounded)
        {
            gun_animator.SetBool("Falling", false);
            //Debug.Log("Grounded");
        }
        else
        {
            gun_animator.SetBool("Falling", true);
            //Debug.Log("InAir");
        }


        if (Input.GetButtonDown("Fire1") && !gun_animator.GetCurrentAnimatorStateInfo(0).IsName("out_of_purse"))
        {
            Shoot();
        }

    }

    // Shooting function
    void Shoot()
    {
        if (reloading)
            return;

        if (magazine <= 0)
        {
            can_shoot = false;
            if (playerInventory.ammunition[typeOfAmmo] > 0)
            {
                gun_animator.SetTrigger("Reloading");
                reloading = true;
                StartCoroutine(reload());
                return;
            }
        }

        if (can_shoot)
        {
            gun_animator.SetTrigger("Shoot");

            RaycastHit hit;
            Ray ray = new Ray();


            ray.direction = fpsCam.transform.forward;
            ray.origin = fpsCam.transform.position;


            if (Physics.Raycast(ray, out hit, range, ~LayerMask.GetMask("Player")))
            {
                print("COLLIDER: " + hit.collider.name);
                SpawnTeleport(hit.point - ray.direction);
            }
            else
            {
                SpawnTeleport(fpsCam.transform.position + fpsCam.transform.forward * range);
            }


            ///////////////////////////////////// SHOOTING ANIMATION CONTROL/////////////////////////////////////////////////////////////////

            StopCoroutine(coroutine);
            gun_animator.SetBool("Steady", true);
            coroutine = waiting();
            StartCoroutine(coroutine);
            //////////////////////////////////////////COOL DOWN//////////////////////////////////////////////////////////////

            StartCoroutine("coolDown");
        }

    }

    // spawns teleport, if spawning teleport without massive collision is failed, teleport is not spawned
    private void SpawnTeleport(Vector3 position)
    {
        if (teleport != null)
        {
            Destroy(teleport);
        }

        bool check = false;


        teleport = Instantiate(prefab, position, fpsCam.transform.rotation);
        
        teleport.GetComponent<NextLevel>().anim = levelLoader;
        teleport.GetComponent<NextLevel>().inv = playerInventory;
        teleport.GetComponent<NextLevel>().start_text = start_text;
        teleport.GetComponent<NextLevel>().end_text = end_text;


        if (Physics.Raycast(teleport.transform.position, teleport.transform.up, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, -teleport.transform.up, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, -teleport.transform.right, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, teleport.transform.right, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, (teleport.transform.up + teleport.transform.right).normalized, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, (teleport.transform.up - teleport.transform.right).normalized, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, (-teleport.transform.up + teleport.transform.right).normalized, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, (-teleport.transform.up - teleport.transform.right).normalized, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }
        else if (Physics.Raycast(teleport.transform.position, (-teleport.transform.up - teleport.transform.right).normalized, radius, ~LayerMask.GetMask("Player", "PickableItem", "Weapon", "Teleport")))
        {
            check = true;
        }


        if (check)
        {
            Destroy(teleport);
        }


    }


    // Waits fixed time before playing movements animation, in order to make shooting animation more smooth 
    IEnumerator waiting()
    {
        float t = 0;
        while (t < coolDownTime * 3)
        {
            yield return new WaitForFixedUpdate();
            // Debug.Log("coroutine_running");
            t += Time.deltaTime;
        }
        gun_animator.SetBool("Steady", false);

    }

    // reloading process
    IEnumerator reload()
    {
        Debug.Log("Reloading...");

        while (!gun_animator.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            yield return new WaitForFixedUpdate();
        }
        while (gun_animator.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            yield return new WaitForFixedUpdate();
        }

        if (playerInventory.ammunition[typeOfAmmo] >= magazineCapacity)
        {
            playerInventory.ammunition[typeOfAmmo] -= magazineCapacity;
            magazine = magazineCapacity;
        }
        else
        {
            magazine = playerInventory.ammunition[typeOfAmmo];
            playerInventory.ammunition[typeOfAmmo] = 0; ;
        }
        reloading = false;
        can_shoot = true;

        fuelTank.SetActive(true);
        
        Debug.Log("Finished Reloading.");
    }

    // doesnt let you shoot too often
    IEnumerator coolDown()
    {
        can_shoot = false;
        yield return new WaitForSeconds(coolDownTime);
        can_shoot = true;
    }
}
