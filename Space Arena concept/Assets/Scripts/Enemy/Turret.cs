using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

/*
 * The script responsible for the behavior of the turret in the game
 */
public class Turret : MonoBehaviour, IDamageable
{
   
    private Transform target;
    [Header("Attributes")] 
    public float health;
    public float range = 15f;
    public float rotationSpeed;
    public float fireRate;
    private float fireCountDown = 0f;
    
    [Header("Unity Setup Fields")]
    public string playerTag = "Player";
    public Transform partToRotate;
    public Transform[] patroliingPoints;
    public TurretGun gunScript;
    
    [Header("Droped loot from enemy")] public GameObject[] itemsDroppedAfterDeath = null;
    public GameObject Base, body, vilka, wire01, wire02;
    public ParticleSystem deathParticles;

    public float timeToChange = 2f;
    private float timeToDown = 0f;
    private Transform point;
    private bool changeDir = true;
    
    // Start is called before the first frame update
    private void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
        point = patroliingPoints[0];
        timeToDown = timeToChange;
    }
    
    /*
     * Update target every 0.5 sec
     */
    private void UpdateTarget()
    {
        GameObject player = GameObject.FindGameObjectWithTag(playerTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestPlayer = null;
        
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        
        if (distanceToPlayer < shortestDistance)
        {
            shortestDistance = distanceToPlayer;
            nearestPlayer = player;
        }
        
        if (nearestPlayer != null && shortestDistance <= range)
        {
            target = nearestPlayer.transform;
        }
        else
        {
            target = null;
        }
    }
    /*
     * The method that turns the turret in the direction of the patrol point
     */
    private void StartPatrol(Transform point)
    {
        // Target lock on
        Vector3 dir = point.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * 5).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    // Update is called once per frame
    private void Update()
    {
        if (target == null)
        {
            // Change patrol point 
            if (timeToDown <= 0)
            {
                timeToDown = timeToChange;
                if (changeDir)
                {
                    point = patroliingPoints[1];
                    changeDir = false;
                }
                else
                {
                    point = patroliingPoints[0];
                    changeDir = true;
                }
            }
            timeToDown -= Time.deltaTime; 
            StartPatrol(point);
        }
        else
        {
            // Target lock on
            Vector3 dir = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
            partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
            
            // Cooldown for shooting
            if (fireCountDown <= 0f)
            {
                RaycastHit hit;
                if (Physics.Raycast(patroliingPoints[0].position, (target.position - patroliingPoints[0].position),out hit, Mathf.Infinity,
                    ~LayerMask.GetMask("Enemy")))
                {
                    if(hit.transform.CompareTag("Player"))
                        Shoot(true);
                }
                fireCountDown = 1f / fireRate;
            }

            fireCountDown -= Time.deltaTime;
        }
    }

    private void Shoot(bool seePlayer)
    {
        gunScript.Shoot(target.transform.position, seePlayer);
    }
    
    public void TakeDamage(float damage)
    {
        if (health > 0)
        {
            health -= damage;
            if (health <= 0f)
            {
                //Invoke("ShowItemsAfterDeath", 1.2f);
                // Disintegration into parts
                Instantiate(Base, transform.position, Quaternion.identity);
                Instantiate(body, transform.position, Quaternion.identity);
                Instantiate(vilka, transform.position, Quaternion.identity);
                Instantiate(wire01, transform.position, Quaternion.identity);
                Instantiate(wire02, transform.position, Quaternion.identity);
                Die();
            }
        }
        
    }
    
    private void ShowItemsAfterDeath()
    {
        foreach (var item in itemsDroppedAfterDeath)
        {
            item.SetActive(true);
        }
        Destroy(GetComponent<CapsuleCollider>());
        transform.GetComponent<MeshRenderer>().enabled = false;
    }


    private void Die()
    {
        Destroy(this.gameObject);
    }
    
    //Debug
    /*
     *  void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
    */
}
     
   
