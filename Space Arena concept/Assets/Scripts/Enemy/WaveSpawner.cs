using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * The script responsible for the appearance of waves
 */

public class WaveSpawner : MonoBehaviour
{
    public static int robotnumber = 0;
    public static bool lastWave = false;

    public enum SpawnStates
    {
        SPAWNING,
        WAITING,
        COUNTING,
        STOP
    };

    [System.Serializable]
    // Wave parameters
    public class Wave
    {
        public string name;
        public Transform[] enemy;
        public int[] count;
    }

    public Wave[] waves;
    private int nextWave = 0;

    public Transform[] spawnPointsForEnemies;
    public Transform[] spawnPointsForTurrets;
    public GameObject[] patrollingPointForEnemies;
    public ParticleSystem spawnEnemyParticle;
    public Transform playerPos;
    public HealthController healthController;
    public ArmorController armorController;

    public float timeBetweenWaves = 5.0f;
    private float waveCountDown;

    private SpawnStates state = SpawnStates.COUNTING;
    private float searchCountDown = 1f;
    public AudioClip[] tracks;
    private int idx = 0;
    private int idx2 = 0;

    private void Start()
    {
        if (spawnPointsForEnemies.Length == 0 || spawnPointsForTurrets.Length == 0)
            Debug.LogError("No spawn points reference!");

        if (waves.Length == 0)
            Debug.LogError("No waves reference!");

        waveCountDown = timeBetweenWaves;
    }

    private void Update()
    {
        
        if (state == SpawnStates.STOP) return;
        if (state == SpawnStates.WAITING)
        {
            // Check if enemies are still alive
            if (!EnemyIsAlive())
            {
                // Begin a new round

                BeginNewRound();
            }
            else
            {
                // Kill all enemies before spawning new wave
                return;
            }
        }

        if (waveCountDown <= 0) // Time to start spawning waves
        {
            if (nextWave == waves.Length - 1)
            {
                lastWave = true;
            }
            if (state != SpawnStates.SPAWNING)
            {
                // Start spawning waves
                SpawnWave(waves[nextWave]);
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }
    }

    private void BeginNewRound()
    {
        state = SpawnStates.COUNTING;
        waveCountDown = timeBetweenWaves;
        

        if (nextWave + 1 > waves.Length - 1)
        {
            // All waves are completed, end.
            //lastWave = true;
            nextWave = 0;
            state = SpawnStates.STOP;
        }
        else
        {
            AudioManager.instance.SwapTrack(tracks[nextWave]);
            nextWave++;
            
        }

    }

    /*
     * Check if enemies are alive
     */
    private bool EnemyIsAlive()
    {
        searchCountDown -= Time.deltaTime;
        if (searchCountDown <= 0f) // Check if we spawn some enemies
        {
            searchCountDown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null && GameObject.FindGameObjectWithTag("Turret") == null)
            {
                return false;
            }
        }
        return true;
    }

    private void SpawnWave(Wave _wave)
    {
        state = SpawnStates.SPAWNING;
        robotnumber = _wave.count[0];
        // Spawn
        for (int i = 0; i <= _wave.enemy.Length - 1; i++)
        {
            for (int j = 0; j < _wave.count[i]; j++)
            {
                SpawnEnemy(_wave.enemy[i]);
            }
        }
        // Done spawning
        state = SpawnStates.WAITING;
    }

    private void SpawnEnemy(Transform _enemy)
    {
        //Spawn enemy
        if (_enemy.CompareTag("Turret"))
        {
            Transform _sp = spawnPointsForTurrets[idx++];
            if (idx == spawnPointsForTurrets.Length) idx = 0;
            Instantiate(spawnEnemyParticle, _sp.position, _sp.rotation);
            Transform gm = Instantiate(_enemy, _sp.position, _sp.rotation);
            // it is necessary for the correct operation of the turret
            gm.GetComponent<TurretGun>().hpController = healthController;
            gm.GetComponent<TurretGun>().armorController = armorController;
        }
        else if (_enemy.CompareTag("Enemy"))
        {
            Debug.Log("Spawning enemy: " + _enemy.name);
            Transform _sp = spawnPointsForEnemies[UnityEngine.Random.Range(0, spawnPointsForEnemies.Length)];
            Instantiate(spawnEnemyParticle, _sp.position, _sp.rotation);
            Transform gm = Instantiate(_enemy, _sp.position, _sp.rotation);
            // it is necessary for the correct operation of the enemies 
            gm.GetComponent<EnemyAIController>().playerPosition = playerPos;
            gm.GetComponent<EnemyGun>().hpController = healthController;
            gm.GetComponent<EnemyGun>().armorController = armorController;

            gm.GetComponent<EnemyAIController>().patrolTargets.Add(patrollingPointForEnemies[idx2++]);
            if (idx2 == patrollingPointForEnemies.Length) idx2 = 0;
        }
    }
}