using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
/*
 * The script responsible for chasing and attacking the player
 */
[CreateAssetMenu(fileName = "ChasingState", menuName = "FSM/Chase")]
public class ChasingState : AbstractState
{
    public override void EnterState(EnemyAIController agent)
    {
        //agent.animator.SetBool("isRunning", true);
        agent.RotateTowards();
        agent.agent.SetDestination(agent.playerPosition.position);
    }

    public override void Execute(EnemyAIController agent)
    {
        if (!agent.canSeePlayer())
        {
            //If can't see player, start looking around
            agent.TransitionState(agent.LookingAroundState);
            return;
        }
        
        agent.RotateTowards();
        agent.agent.SetDestination(agent.playerPosition.position);
        
        // If player in front of enemy, enemy can shoot
        if (Vector3.Dot(agent.transform.TransformDirection(Vector3.forward),
                        (agent.playerPosition.position - agent.transform.position).normalized) >
                    Mathf.Cos(90 / 2.0f))
        {
            agent.agent.transform.LookAt(agent.playerPosition, Vector3.up);    
            agent.gun.Shoot(agent.playerPosition.position);
        }

        //Enemy stops when player in attack range
        if (agent.isPlayerInAttackRange())
        {
            //Turn to the player
            agent.RotateTowards();
            agent.TransitionState(agent.EvadeState);
        }
    }
}