using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.AI;
/*
 * The script responsible for attacking the player and evading
 */

[CreateAssetMenu(fileName = "EvadeState", menuName = "FSM/Evade")]
public class EvadeState : AbstractState
{
    private float walkSpeed;
    private float timeToStopped;
    private Vector3 axis;
    public float timeToWait = 3f;
    private bool changeDir = true;
    public override void EnterState(EnemyAIController agent)
    {
        //Start
        //agent.animator.SetBool("isRunning", false);
        agent.agent.isStopped = true;
        timeToStopped = 0;
        axis = Vector3.up;
    }

    public override void Execute(EnemyAIController agent)
    {
        if (!agent.canSeePlayer())
        {
            //If can't see player, start looking around
            //Debug.Log("Start looking player");
            agent._shootByPlayer = false;
            agent.agent.isStopped = false;
            agent.TransitionState(agent.LookingAroundState);
            return;
        }
        agent.RotateTowards();
        float distance = Vector3.Distance(agent.transform.position, agent.playerPosition.position);
        
        // If the player is too close to the enemy, move away
        if ( distance < agent.safetyDistance)
        {
            //agent.animator.SetBool("isRunning", true);
            agent.stepAwayFromPlayer();
        }
        
        // If enemy was shoot by player, start dodge.
        if (agent._shootByPlayer)
        {
            // Change direction after 3 seconds
            if (timeToStopped <= 0)
            {
                timeToStopped = timeToWait;
                if (changeDir)
                {
                    axis = Vector3.down;
                    changeDir = false;
                }
                else
                {
                    axis = Vector3.up;  
                    changeDir = true;
                }
            }
            timeToStopped -= Time.deltaTime;
            //agent.animator.SetBool("isRunning", true);
            agent.agent.transform.RotateAround(agent.playerPosition.position, axis,20*Time.deltaTime*agent.dodgeSpeed);
            agent.gun.Shoot(agent.playerPosition.position);
        }
        
        // If player in front of enemy, enemy can shoot
        if (Vector3.Dot(agent.transform.TransformDirection(Vector3.forward),
                        (agent.playerPosition.position  - agent.transform.position).normalized) >
                    Mathf.Cos(90 / 2.0f))
        {
            //Debug.Log(agent.agent.transform.name);
            agent.agent.transform.LookAt(agent.playerPosition, Vector3.up);
            agent.gun.Shoot(agent.playerPosition.position);
        }
    }
}
