using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * The script responsible for looking the player
 */
[CreateAssetMenu(fileName = "LookingAroundState", menuName = "FSM/LookingAround")]
public class LookingAroundState : AbstractState
{
    private float timeStopped;
    private bool reached = false;
    public float timeToWait = 5.0f;

    public override void EnterState(EnemyAIController agent)
    {
        //agent.animator.SetBool("isRunning", true);
        reached = false;
    }

    public override void Execute(EnemyAIController agent)
    {
        if (agent.canSeePlayer())
        {
            agent.TransitionState(agent.AlertState);
            return;
        }
        
        // If there is no player in the place where enemy last saw him, then go back to the patrol
        if (reached && ((Time.time - timeStopped) > timeToWait))
        {
            agent.TransitionState(agent.PatrolState);
            return;
        }
        
        if (agent.hasReachedDestination() && !reached)
        {
            reached = true;
            timeStopped = Time.time;
            //agent.animator.SetBool("isRunning", false);
        }
    }
}