using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * The script responsible for patrolling localities
 */
[CreateAssetMenu(fileName = "PatrollingState", menuName = "FSM/Patrol")]
public class PatrollingState : AbstractState
{
    public override void EnterState(EnemyAIController agent)
    {
        
    }

    public override void Execute(EnemyAIController agent)
    {
        if (agent.canSeePlayer())
        {
            agent.TransitionState(agent.AlertState);
        }
        
        if (agent.hasReachedDestination())
        {
            // Get random patroll point
            agent.currentTarget = agent.patrolTargets[agent.rnd.Next(agent.patrolTargets.Count)].transform.position;
        }

        // Go to the patroll point
        agent.agent.SetDestination(agent.currentTarget);
    }
}
