using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.ProBuilder.MeshOperations;
/*
 * A script that controls the behavior of the enemy in the game
 */
public class EnemyAIController : MonoBehaviour, IDamageable
{
    [Header("Main parameters")] 
    [SerializeField] public NavMeshAgent agent;
    [SerializeField] private float health;
    [SerializeField] private float attackDist;
    //[SerializeField] private float seeingAngle;
    [SerializeField] private float seeingDist;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float nearTargetDistance;
    public float safetyDistance = 5f;
    public float dodgeSpeed;

    private AbstractState currentState;
    [Header("Reference")]
    public AbstractState PatrolState;
    public AbstractState AlertState;
    public AbstractState LookingAroundState;
    public AbstractState EvadeState;
    public LayerMask whatIsPlayer;
    public Transform playerPosition;
    public EnemyGun gun;

    public List<GameObject> patrolTargets;
    public Vector3 currentTarget;
    public bool _shootByPlayer = false;

    [Header("Droped loot from enemy")] public GameObject[] itemsDroppedAfterDeath = null;
    public GameObject[] detatils;
    public ParticleSystem deathParticles;

    public System.Random rnd = new System.Random();
    public GameObject teleportationFuel;

    private void Awake()
    {   
        // optional
        //animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        currentState = PatrolState;
        currentState.EnterState(this);
        currentTarget = patrolTargets[rnd.Next(patrolTargets.Count)].transform.position;
    }
    
    private void Update()
    {
        currentState.Execute(this);
    }
    
    /*
     * Check if Enemy got to the point.
     * return: True - enemy reached the target, False - enemy not reached the target.
     */
    public bool hasReachedDestination()
    {
        if (Vector3.Distance(currentTarget, transform.position) <= nearTargetDistance)
        {
            if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
            {
                return true;
            }
        }
        return false;
    }
    
    /*
   * Check if Enemy can see player.
   * return: True - enemy see the player, False - enemy don't see the player.
   */
    public bool canSeePlayer()
    {
        // Part of the code with an angle visibility check starts
        /*
        RaycastHit hit;

        if (Vector3.Dot(transform.TransformDirection(Vector3.forward), (playerPosition.position + new Vector3(0.0f, 1.0f, 0.0f) - transform.position).normalized) < Mathf.Cos(360 / 2.0f)) return false;
        if (Physics.Raycast(transform.position, (playerPosition.position + new Vector3(0.0f, 1.0f, 0.0f) - transform.position).normalized, out hit, seeingDist, ~LayerMask.GetMask("Enemy")))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                Debug.Log("See player");
                return true;
            }
        }
        Debug.Log("Dont See player");
        return false;
        */
        // Part of the code with an angle visibility check ends
        
        // Send hit in seeing distance of enemy
        RaycastHit hit;
        if (Physics.Raycast(transform.position, (playerPosition.position - transform.position).normalized, out hit, seeingDist, ~LayerMask.GetMask("Enemy")))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                //Debug.Log("See player");
                //Debug.DrawRay(transform.position, (playerPosition.position - transform.position).normalized * hit.distance, Color.yellow);
                return true;
            }
        }
        //Debug.Log("Dont see player");
        return false;
    }
    
    /*
     * A method for switching between states
     * Param: new state
     */
    public void TransitionState(AbstractState state)
    {
        currentState = state;
        currentState.EnterState(this);
    }
    
    public void TakeDamage(float damage)
    {
        if(health > 0)
        {
            health -= damage;
            /*
                * If enemy took damage, start to chase the player 
                */
            _shootByPlayer = true;
            TransitionState(AlertState);
            if (health <= 0f)
            {
                // Disintegration into parts
                foreach (GameObject detail in detatils)
                {
                    Instantiate(detail, transform.position, Quaternion.identity);
                }
                WaveSpawner.robotnumber--;
                Die();
            }
        }
        
    }
    
    /*
     * A method that turns the enemy towards the player.
     */
    public void RotateTowards()
    {
        Vector3 direction = (playerPosition.position - agent.transform.position);
        Quaternion lookRotation = Quaternion.LookRotation(direction); 
        Vector3 rotation = Quaternion.Lerp(agent.transform.rotation, lookRotation, Time.deltaTime * 5f).eulerAngles;
        agent.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    /*
     * A method to check if an enemy can hit a player
     * return: True - when player in range, false - otherwise
     */
    public bool isPlayerInAttackRange()
    {
        bool playerinAttackRange = Physics.CheckSphere(transform.position, attackDist, whatIsPlayer);
        return playerinAttackRange;
    }
    
    /*
    * A method to maintain the distance between the player and the enemy.
    */
    public void stepAwayFromPlayer()
    {
        agent.isStopped = false;
        Vector3 dirToPlayer = transform.position - playerPosition.position;
        Vector3 newPos = transform.position + dirToPlayer;
        agent.SetDestination(newPos);
        RotateTowards();
    }
    
    /*
     * The method responsible for the loss of loot from enemies.
     */
    private void ShowItemsAfterDeath()
    {
        foreach (var item in itemsDroppedAfterDeath)
        {
            float a = UnityEngine.Random.Range(-1f, 1f);

            if (a > 0f)
            {
                var g = Instantiate(item);
                g.transform.position = transform.position;
                g.GetComponent<Rigidbody>().AddForce( new Vector3(a, a, a), ForceMode.Impulse);
            }
        }
        Debug.Log("last wave: " + WaveSpawner.lastWave);
        if (WaveSpawner.lastWave && WaveSpawner.robotnumber == 0)
        {
            var g = Instantiate(teleportationFuel);
            g.transform.position = transform.position;
            print("I AM THE LAST OF MY BLOODLINE");
        }
        //Destroy(GetComponent<CapsuleCollider>());
        //transform.GetComponent<MeshRenderer>().enabled = false;
    }
    
    private void Die()
    {
        ShowItemsAfterDeath();
        Destroy(this.gameObject);
        
    } 
    
    // Debug   
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(agent.transform.position, seeingDist);
            Gizmos.DrawWireSphere(agent.transform.position, attackDist);
            Gizmos.DrawLine(transform.position, playerPosition.position + new Vector3(0.0f, 1.0f, 0.0f));
        }
    }
#endif
}





