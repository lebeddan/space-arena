using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * The script responsible for the use of force on game objects
 */
public class AddForce : MonoBehaviour
{
    public Rigidbody rb;
    public float force;
    public Vector3 dir;
    public float timeBeforeDestroy = 4f;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(dir* force, ForceMode.Force);
        Destroy(gameObject, timeBeforeDestroy);
    }
}