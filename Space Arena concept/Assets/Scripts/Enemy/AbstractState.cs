using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractState : ScriptableObject
{
    public abstract void EnterState(EnemyAIController agent);
    public abstract void Execute(EnemyAIController agent);
}
