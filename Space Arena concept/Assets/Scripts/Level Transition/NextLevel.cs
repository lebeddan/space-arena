using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/// <summary>
///  Loads next or firs scene if object collides with player objects 
/// </summary>
public class NextLevel : MonoBehaviour
{
    public Animator anim;   // level loading animation
    public Inventory inv;   // player inventory class
    public float secondsOfWaiting = 5f; // minimal time of level loading animation
    public Text start_text, end_text;   // level loading text

    private bool waiting = false;   

    private bool pauseForReading = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && !waiting)
        {
            if (inv.ammunition[Ammunition.teleportationFuel] > 0)
                inv.ammunition[Ammunition.teleportationFuel]--;
            
            start_text.gameObject.SetActive(false);
            end_text.gameObject.SetActive(true);

            

            anim.SetTrigger("transition");
            inv.transform.GetComponent<PlayerMovement>().enabled = false;
            DisableWeapons(inv.transform);
            DisableCameraMovement(inv.transform);
            StartCoroutine(ReadingPause());

            
        }
    }

    private void Update()
    {
        if (!pauseForReading && waiting)
        {
            LoadNext();
        }
    }


    /// <summary>
    /// Loads scene
    /// </summary>
    private void LoadNext()
    {


        if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCount)
        {
            //print("This Scene: " + SceneManager.GetActiveScene().buildIndex + " Scenes at all " + SceneManager.sceneCount + " Loading next ...");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            //print("Loading scene zero");
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene(0);
        }

        waiting = false;
    }

    /// <summary>
    /// prevents program from loading scene immediately
    /// </summary>
    private IEnumerator ReadingPause()
    {
        pauseForReading = true;

        yield return new WaitForSeconds(secondsOfWaiting);

        pauseForReading = false;
        waiting = true;
    }


    /// <summary>
    ///     Disables object with WeaponSwitching compoonent in children of Transform parameter
    /// </summary>
    private void DisableWeapons(Transform t)
    {
        foreach (Transform child in t)
        {
            var a = child.GetComponent<WeaponSwitching>();
            if (a != null)
            {
                a.enabled = false;
            }
            else
            {
                DisableWeapons(child);
            }
        }
    }

    /// <summary>
    ///     Disables MouseLook component compoonent in children of Transform parameter
    /// </summary>
    private void DisableCameraMovement(Transform t)
    {
        foreach (Transform child in t)
        {
            var a = child.GetComponent<MouseLook>();
            if (a != null)
            {
                a.enabled = false;
            }
            else
            {
                DisableCameraMovement(child);
            }
        }
    }

}

