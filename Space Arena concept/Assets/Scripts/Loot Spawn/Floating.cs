using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Makes object float above ground upon reaching it
/// </summary>
public class Floating : MonoBehaviour
{
    private bool floatingState = false;

    public float floatingSpeed = 0.5f;  // speed of floating
    public float floatingDelta = 0.1f;  // how far from central position object moves when floating


    private void OnTriggerEnter(Collider other)
    {
        if (!floatingState && other.gameObject.layer.Equals(LayerMask.NameToLayer("Ground")))
        {
            print("True");
            StartCoroutine(floating());

            transform.GetComponent<Rigidbody>().isKinematic = true;
        }
    }


    /// <summary>
    ///     floating process
    /// </summary>
    private IEnumerator floating()
    {
        floatingState = true;
        bool movementUp = false;
        float lastPos = 0;
        Vector3 origPos = new Vector3(transform.position.x, transform.position.y + floatingDelta * 2, transform.position.z);

        while (true)
        {
            float currentPos = 0;

            if (movementUp)
            {
                currentPos = lastPos + Time.deltaTime * floatingSpeed;

                if (currentPos >= floatingDelta)
                {
                    currentPos = floatingDelta;
                    movementUp = false;
                }

            }
            else
            {
                currentPos = lastPos - Time.deltaTime * floatingSpeed;

                if (currentPos <= -floatingDelta)
                {
                    currentPos = -floatingDelta;
                    movementUp = true;
                }

            }


            transform.position = new Vector3(transform.position.x, origPos.y + currentPos, transform.position.z);

            lastPos = currentPos;

            yield return new WaitForFixedUpdate();
        }
    }
}
