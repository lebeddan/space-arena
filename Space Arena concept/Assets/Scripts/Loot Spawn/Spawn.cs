using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Spqwn point class
/// </summary>
public class Spawn : MonoBehaviour
{
    public int index = -1;                      //  index of spawn point, is set by spawnController 
    public itemSpawnController controller;      //  Spawn Controller class that controls spawn point
    bool itemSpawned = true;                    
    bool waiting = false;                       
    bool floatingState = false;
    float waitingTime = 3f;                 

    public float floatingSpeed = 0.5f;          //  speed of spawned object floating
    public float floatingDelta = 0.1f;          //  how far from center floating object moves

    private void Update()
    {

        if (index != -1)
        {
            if (itemSpawned)
            {
                if (transform.childCount == 0)
                {
                    itemSpawned = false;
                    waiting = true;
                    StartCoroutine("wait");
                }
            }
            else if (waiting)
            {
                if (transform.childCount != 0)
                {
                    itemSpawned = true;
                    waiting = false;
                }
            }
        }
        if (!floatingState)
        {
            StartCoroutine(floating());
        }
    }


    /// <summary>
    /// makes spawned object float
    /// </summary>
    private IEnumerator floating()
    {
        floatingState = true;
        bool movementUp = false;
        float lastPos = 0;
        Transform child = null;
        Vector3 origPos  = new Vector3(0,0,0);
        if (transform.childCount > 0)
        {
            child = transform.GetChild(0);
            origPos = child.position;
        }

        while (transform.childCount != 0)
        {
            float currentPos = 0;

            if (movementUp)
            {
                currentPos = lastPos + Time.deltaTime * floatingSpeed;

                if (currentPos >= floatingDelta)
                {
                    currentPos = floatingDelta;
                    movementUp = false;
                }

            }
            else
            {
                currentPos = lastPos - Time.deltaTime * floatingSpeed;

                if (currentPos <= -floatingDelta)
                {
                    currentPos = -floatingDelta;
                    movementUp = true;
                }

            }

            child.position = new Vector3(child.position.x, origPos.y + currentPos, child.position.z);

            lastPos = currentPos;

            //print("currnet delta: " + currentPos);

            yield return new WaitForFixedUpdate();
        }

        floatingState = false;
    }

    // waits given time before requesting new object
    private IEnumerator wait()
    {
        yield return new WaitForSeconds(waitingTime);
        requestItem();
    }

    // requests new object from controller
    private void requestItem()
    {
        controller.GenerateItem(index);
    }

}
