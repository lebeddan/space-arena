using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// takes spawn points, object to be spawned and their ration of spawn and controls spawning process
/// </summary>
public class itemSpawnController : MonoBehaviour
{
    public List<Transform> Spawns = new List<Transform>();  // spawn points
    public List<GameObject> Items = new List<GameObject>(); //  objects to spawn
    public List<int> Values = new List<int>();              //  spawn rate, (if -1 then is always spqned at least at one place)

    private int quantity = 0;
    private List<int> present;
    private List<int> vitals = new List<int>();
    private System.Random rnd = new System.Random();
    private List<int> memory;

    private void Awake()
    {
        quantity = Mathf.Min(Values.Count, Items.Count);
        present = new List<int>(quantity);
      
        memory = new List<int>(Spawns.Count);

       // Debug.Log("spawned: " + spawned.Count + ",Spawns:" + Spawns.Count + ", "); 
     
      
        for(int i = 0; i < Spawns.Count; i++)
        {
            
            memory.Add(-1);
            Spawns[i].gameObject.GetComponent<Spawn>().index = i;
        }

        for(int i = 0; i < quantity; i++)
        {
            present.Add(0);

            if (Values[i] == -1)
            {
                vitals.Add(i);
                Values[i] = 10;
            }
        }
    }

    // instantiates chosen randomly object on spawnId spawn point
    public void GenerateItem(int spawnId)
    {

        if (memory[spawnId] >= 0)
        {
            present[memory[spawnId]]--;
        }

        GameObject ob = Choose();
        Transform spawn = Spawns[spawnId];
        GameObject g = Instantiate(ob, new Vector3(spawn.position.x, spawn.position.y, spawn.position.z), Quaternion.identity);
       
        g.transform.SetParent(spawn);
        Rigidbody rb = g.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.isKinematic = true;
        }
        
        memory[spawnId] = Items.IndexOf(ob);
    }

    // chooses object to spawn
    private GameObject Choose()
    {
        List<GameObject> choices = new List<GameObject>();
        List<int> weights = new List<int>();
        GameObject g;
        foreach (var index in vitals)
        {
            if (present[index] < 1)
            {
                choices.Add(Items[index]);
                weights.Add(1);
            }
        }
        if (choices.Count > 0)
        {
            g = weigthedRand(choices, weights);
            present[Items.IndexOf(g)]++;
            return g;
        }
        g = weigthedRand(Items, Values);
        present[Items.IndexOf(g)]++;
        return g;
    }

    // returns random objects from list
    private GameObject weigthedRand(List<GameObject> choices,List<int> weights)
    {
        var cumulativeWeight = new List<int>();
        int last = 0;
        foreach (var cur in weights)
        {
            last += cur;
            cumulativeWeight.Add(last);
        }
        
        int choice = rnd.Next(last);
        
        for(int i =0; i < cumulativeWeight.Count; i++)
        {
            if (choice < cumulativeWeight[i])
            {
                //Debug.Log("choice: " + choices[i].name);
                return choices[i];
            }
        }
        return null;
    }

}
