using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalAchievemnts : MonoBehaviour
{
    [Header("General Variables")]
    public GameObject achNote;
    public AudioSource achSound;
    public GameObject achTitle;
    public GameObject achDesc;
    public bool achActive = false;
    
    [Header("01Achievemnt Specifics")]
    public GameObject ach01Img;
    public int ach01Code;
    
    [Header("02Achievemnt Specifics")]
    public GameObject ach02Img;
    public static bool triggerAch02 = false;
    public int ach02Code;
    
    
    private void Start()
    {
        StartCoroutine(TriggerAch01());
    }
    
    // Update is called once per frame
    private void Update()
    {
        ach01Code = PlayerPrefs.GetInt("Ach01");
        ach02Code = PlayerPrefs.GetInt("Ach02");
        if (ach01Code != 12345)
        {
            StartCoroutine(TriggerAch01());
        }

        if (triggerAch02 == true && ach02Code != 12346)
        {
            StartCoroutine(TriggerAch02());
        }
    }

    IEnumerator TriggerAch01()
    {
        achActive = true;
        ach01Code = 12345;
        PlayerPrefs.SetInt("Ach01", ach01Code);
        achSound.Play();
        ach01Img.SetActive(true);
        achTitle.GetComponent<Text>().text = "It's work!!!";
        achDesc.GetComponent<Text>().text = "Start the game so that it does not crash";
        achNote.SetActive(true);
        yield return new WaitForSeconds(7);
        
        // Resetting UI
        achNote.SetActive(false);
        ach01Img.SetActive(false);
        achTitle.GetComponent<Text>().text = "";
        achDesc.GetComponent<Text>().text = "";
        achActive = false;
    }
    
    IEnumerator TriggerAch02()
    {
        achActive = true;
        ach02Code = 12346;
        PlayerPrefs.SetInt("Ach02", ach01Code);
        achSound.Play();
        ach02Img.SetActive(true);
        achTitle.GetComponent<Text>().text = "Completed!!!";
        achDesc.GetComponent<Text>().text = "Finish 1 level";
        achNote.SetActive(true);
        yield return new WaitForSeconds(7);
        
        // Resetting UI
        achNote.SetActive(false);
        ach02Img.SetActive(false);
        achTitle.GetComponent<Text>().text = "";
        achDesc.GetComponent<Text>().text = "";
        achActive = false;
    }
}
