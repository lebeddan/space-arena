using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * The script responsible for the transition of tracks in the game
 */
public class AudioManager : MonoBehaviour
{
    public AudioSource defaultAmbience;

    private AudioSource track01, track02;
    private bool isPlayingTrack01;

    public static AudioManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        track01 = gameObject.GetComponent<AudioSource>();
        track02 = gameObject.GetComponent<AudioSource>();
        isPlayingTrack01 = true;
    }

    public void SwapTrack(AudioClip newClip)
    {
        StopAllCoroutines();
        StartCoroutine(FadeTrack(newClip));
        isPlayingTrack01 = !isPlayingTrack01;
    }

    public void ReturnToDefault()
    {
        //SwapTrack(defaultAmbience);
    }

    private IEnumerator FadeTrack(AudioClip newClip)
    {
        float timeToFade = 3.25f;
        float timeElapsed = 0;

        if (isPlayingTrack01)
        {
            Debug.Log("TEue");
            track01.clip = newClip;
            track01.Play();

            while (timeElapsed < timeToFade)
            {
                //Fade in track01 and fade out track02
                track01.volume = Mathf.Lerp(0, 0.5f, timeElapsed / timeToFade);
                Debug.Log(track01.name);
                Debug.Log(defaultAmbience.name);
                defaultAmbience.volume = Mathf.Lerp(1, 0, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }

            defaultAmbience.Stop();
        }
        else
        {
            while (timeElapsed < timeToFade)
            {
                track01.volume = Mathf.Lerp(0.5f, 0, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }
            track01.Stop();
            Debug.Log("ELse");
            track02.clip = newClip;
            track02.Play();
            timeElapsed = 0;
            while (timeElapsed < timeToFade)
            {
                track02.volume = Mathf.Lerp(0, 0.5f, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }
        }
    }
}
