using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * The script responsible for causing damage to the player
 */
public class DamageController : MonoBehaviour
{
    [SerializeField] private float Damage = 10.0f;
    [SerializeField] private float DamageOnArmor = 5.0f;
    
    [SerializeField] private GameObject explosiveParticle = null;

    [SerializeField] private HealthController hpController = null;
    
    [SerializeField] private ArmorController armorController = null;

   // [SerializeField] private AudioClip damageAudio = null;

   // private bool playingAudio;
  //  private AudioSource enemyAudioSource;

    private void Start()
    {
       // enemyAudioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            // if player hits something, take damage
            explosiveParticle.SetActive(true);
            // enemyAudioSource.PlayOneShot(damageAudio);
            if (hpController.currentPlayerHP > 0)
            {
                hpController.currentPlayerHP -= Damage;
                if (hpController.currentPlayerHP <= 0) 
                    hpController.currentPlayerHP = hpController.maxPlayerHP;
                else
                    hpController.TakeDamage();
            }
            
            if (armorController.currentPlayerArmor > 0)
            {
                armorController.currentPlayerArmor -= Damage + DamageOnArmor;
                if (armorController.currentPlayerArmor <= 0)
                    armorController.currentPlayerArmor = 0;
                else
                    armorController.TakeDamage();
            }
            
            gameObject.GetComponent<BoxCollider>().enabled = true;
            //playingAudio = true;
        }
    }

    private void Update()
    {
        /*
        if(playingAudio)
        {
            if(!enemyAudioSource.isPlaying)
            {
                gameObject.SetActive(false);
            }
        }
        */
    }
}

