
using UnityEngine;

/// <summary>
/// Player Inventory class
/// </summary>
public class Inventory : MonoBehaviour
{
    public static Inventory instance;

    public int weponHolderCapacity = 5; // how many weapons can player wield
    public int[] ammunition = new int[4];   // quantities of every ammunition type player have
    public int weapons = 1;                 // number of weapons player has
    public Transform fpsCam;                // player main camera
    public Transform weaponCam;             // player weapon camera


    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else 
        {
            instance = this;
        }
    }

}
