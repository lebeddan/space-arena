using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ammunition types class
/// </summary>
public static class Ammunition
{
    public const int lightBullets = 0;

    public const int teleportationFuel = 1;

    public const int laserBattery = 2;

    public const int shotgunAmmo = 3;

}
