using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// class that realizes switching from teleport gun to weapon holder guns and vice versa
/// </summary>
public class SwitchToTeleport : MonoBehaviour
{
    public GameObject tGun;         // teleport gun  in player children
    public GameObject WeaponHolder; // weaponHolder, which children are player weapons


    private void Update()
    {
        if (transform.GetComponent<Inventory>().ammunition[Ammunition.teleportationFuel] == 0)
        {
            if (Input.GetKeyDown("t") && !tGun.activeSelf)
            {
                WeaponHolder.SetActive(false);
                tGun.SetActive(true);
            }
            else if (Input.GetKeyDown("t") && tGun.activeSelf)
            {
                WeaponHolder.SetActive(true);
                tGun.SetActive(false);
            }
        }

        if (transform.GetComponent<Inventory>().ammunition[Ammunition.teleportationFuel] != 0 && !tGun.activeSelf)
        {
            WeaponHolder.SetActive(false);
            tGun.SetActive(true);
        }
    }
}
