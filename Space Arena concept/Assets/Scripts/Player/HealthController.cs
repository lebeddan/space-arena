using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * A script that controls the player's health and fading HUD
 */
public class HealthController : MonoBehaviour
{
    [Header("Player Health Amount")] 
    public float currentPlayerHP = 100.0f;
    public float maxPlayerHP = 100.0f; // shows this var in Unity
    public int playerLives;

    [SerializeField] private int regenRate = 1;
    public bool pickUpAidKit = false;
    [SerializeField] private float FadeRateInForHurtImage;// the min value is 10
    [SerializeField] private float FadeRateOutForHurtImage; // the best value is 5

    [Header("Refernce")] 
    public PlayerManager playerMn;
    [Header("Add the Splatter image here")]
    public Image redSplatterImg = null;

    [Header("Hurt Image Flush")]
    [SerializeField] private Image hurtImg = null;
    [SerializeField] private float hurtTimer = 0.1f;

    [Header("Heal Timer")]
    [SerializeField] private float healCooldown = 3.0f;
    [SerializeField] private float maxHealCooldown = 3.0f;

    [Header("Audio Name")]
    [SerializeField] private AudioClip hurtAudio = null;
    [SerializeField] private AudioClip healAudio = null;
    [SerializeField] private AudioSource healthAuidoSource;
    
    private float fade;
    private bool fadeOut = false;
    private bool canRegen = false;




    // Start is called before the first frame update
    private void Start()
    {
       healthAuidoSource = GetComponent<AudioSource>();
    }
    
    public void UpdateHealth()
    {
        // Change alpha channel depends on player HP
        Color splatterAlpha = redSplatterImg.color;
        splatterAlpha.a = 1 - (currentPlayerHP / maxPlayerHP);
        Debug.Log(splatterAlpha.a);
        redSplatterImg.color = splatterAlpha;
    }
    
    IEnumerator HurtFlash()
    {
        // Fade in hurt image 
        FadeIn();
        healthAuidoSource.PlayOneShot(hurtAudio);
        yield return new WaitForSeconds(hurtTimer);
    }

    public void TakeDamage()
    {
        if(currentPlayerHP >= 0)
        {
            canRegen = false;
            StartCoroutine(HurtFlash());
            // Fade out hurt image
            fadeOut = true;
            UpdateHealth();
        }

        healCooldown = maxHealCooldown;
    }
    
    private void Update()
    {
        if (fadeOut) FadeOut();
        
        if(pickUpAidKit)
        {
            // Start regen after cooldown 
            healthAuidoSource.PlayOneShot(healAudio);
            healCooldown -= Time.deltaTime;
            if(healCooldown <= 0)
            {
                canRegen = true;
                pickUpAidKit = false;
            }
        }

        if(canRegen)
        {
            if(currentPlayerHP <= maxPlayerHP - 0.01)
            {
                currentPlayerHP += Time.deltaTime * regenRate;
                UpdateHealth();
            }
            else
            {
                currentPlayerHP = maxPlayerHP;
                healCooldown = maxHealCooldown;
                canRegen = false;
            }
        }
    }

    private void FadeIn()
    {
        fade = 1.0f;
        Color hurtAlpha = hurtImg.color;
        hurtAlpha.a = Mathf.Lerp(hurtAlpha.a, fade, FadeRateInForHurtImage*Time.deltaTime);
        hurtImg.color = hurtAlpha;
    }

    private void FadeOut()
    {
        fade = 0.0f;
        Color hurtAlpha = hurtImg.color;
        hurtAlpha.a = Mathf.Lerp(hurtAlpha.a, fade, FadeRateOutForHurtImage*Time.deltaTime);
        if (hurtAlpha.a == 0) fadeOut = false;
        hurtImg.color = hurtAlpha;
    }
}
