using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/*
 * the script responsible for the behavior of the armor in the game
 */
public class ArmorController : MonoBehaviour
{
    [Header("Player Armor Amount")] 
    public float currentPlayerArmor = 0;
    [SerializeField] private float maxPlayerArmor = 50.0f; 
    [SerializeField] private float regenRate = 5.0f;
    [SerializeField] public GameObject armor;
    [SerializeField] private float FadeRateInForHurtImage;// the min value is 10
    [SerializeField] private float FadeRateOutForHurtImage; // the best value is 5
    private float fade;
    private bool canRegenArmor = false;
    private bool pickUpArmor = false;
    
    
    [Header("Add the Armor image here")]
    [SerializeField] private Image armorImg = null;

    [Header("Hurt Image Flush")]
    [SerializeField] private Image hurtImg = null;
    [SerializeField] private float hurtTimer = 0.1f;

    [Header("Armor Timer")]
    [SerializeField] private float armorCooldown = 3.0f;

    [SerializeField] private float maxArmorCooldown = 3.0f;

    [Header("Audio Name")]
    [SerializeField] private AudioClip hurtAudio = null;
    [SerializeField] private AudioClip pickUpArmorAudio = null;
    [SerializeField] private AudioSource armorAuidoSource;
    private float fading;
    public float FadeRate;
    public bool haveArmor = false;
    private bool fadeOut;
    
    private void Start()
    {
        armorAuidoSource = GetComponent<AudioSource>();
        fading = armorImg.color.a;
    }
    
    private void UpdateArmor()
    {
        // Change alpha channel depends on 
        Color armorAlpha = armorImg.color;
        armorAlpha.a = Mathf.Lerp(armorAlpha.a, fading, FadeRate*Time.deltaTime);
        armorImg.color = armorAlpha;
    }

    private void FadeIn()
    {
        fading = 1.0f;
        // Fade in hurt image
        Color hurtAlpha = hurtImg.color;
        hurtAlpha.a = Mathf.Lerp(hurtAlpha.a, fading, FadeRateInForHurtImage*Time.deltaTime);
        hurtImg.color = hurtAlpha;
    }
    
    private void FadeOut()
    {
        fading = 0.0f;
        // Fade out hurt image
        Color hurtAlpha = hurtImg.color;
        hurtAlpha.a = Mathf.Lerp(hurtAlpha.a, fading, FadeRateOutForHurtImage*Time.deltaTime);
        if (hurtAlpha.a == 0) fadeOut = false;
        hurtImg.color = hurtAlpha;
    }
    
    IEnumerator HurtFlash()
    {
        // Fade in hurt image
        FadeIn();
        armorAuidoSource.PlayOneShot(hurtAudio);
        yield return new WaitForSeconds(hurtTimer);
        
    }
    
    public void TakeDamage()
    {
    
        if(currentPlayerArmor >= 0)
        {
            canRegenArmor = false;
            StartCoroutine(HurtFlash());
            FadeOut();
            fadeOut = true;
            UpdateArmor();
        }
        else
        {
            currentPlayerArmor = 0;
            haveArmor = false;
        }
        
        armorCooldown = maxArmorCooldown;
    }
    private void Update()
    {
        if (fadeOut) FadeOut();
        if (currentPlayerArmor <= 0) armorImg.enabled = false;
        
        if(pickUpArmor)
        {
            // Start regen after cooldown
            armorCooldown -= Time.deltaTime;
            armorAuidoSource.PlayOneShot(pickUpArmorAudio);
            haveArmor = true;
            if(armorCooldown <= 0)
            {
                armorImg.enabled = true; 
                canRegenArmor = true;
                pickUpArmor = false;
            }
        }

        if(canRegenArmor)
        {
            fading = 1.0f;
            if(currentPlayerArmor <= maxPlayerArmor - 0.01)
            {
                currentPlayerArmor += Time.deltaTime * regenRate;
                UpdateArmor();
            }
            else
            {
                currentPlayerArmor = maxPlayerArmor;
                armorCooldown = maxArmorCooldown;
                canRegenArmor = false;
            }
        }
    }
    
    public void setPickUpArmor(bool var)
    {
        pickUpArmor = var;
    }
}
