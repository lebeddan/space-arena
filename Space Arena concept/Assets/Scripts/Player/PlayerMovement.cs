using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    
    public AudioSource movementAudioSource;
    public AudioClip movementAudio;
    public AudioClip onGroundAudio;
    

    Vector3 velocity;
    public bool isGrounded;

    private void Start()
    {
        movementAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // surface test sphere 
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        
        // moving "WASD"
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = (transform.right * x + transform.forward * z).normalized;

        controller.Move(move * speed * Time.deltaTime);
        
        
        if (isGrounded && controller.velocity.magnitude > 2f && controller.GetComponent<AudioSource>().isPlaying == false)
        {
           // Debug.Log("FOOTSTEPS SOUND!!!");
            controller.GetComponent<AudioSource>().volume = Random.Range(0.4f, 1);
            movementAudioSource.PlayDelayed(0.5f);
            
        }
        
        
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity); // formule
        }

        // gravity
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
}
