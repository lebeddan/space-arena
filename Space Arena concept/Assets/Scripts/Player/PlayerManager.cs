using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
/*
 * The script responsible for linking the player and the rest of the controllers
 */
public class PlayerManager : MonoBehaviour
{
    
    [SerializeField] private HealthController healthController;
    [SerializeField] private ArmorController armorController;
    [SerializeField] private Transform respawnPoint;
    [SerializeField] private Transform player;

    public Animator anim;

    public float secondsOfWaiting = 3f;
    public Text start_text, dead_text;
    private bool pauseForReading = false;
    public bool waiting = false;
    

    public bool PickUpItem(GameObject obj)
    {
        // Behavior for loot
        switch(obj.tag)
        {
            case "HP":
                healthController.pickUpAidKit = true;
                Debug.Log("HP");
                return true;
            case "LightAmmo":
                player.GetComponent<Inventory>().ammunition[Ammunition.lightBullets] += 10;
                return true;
            case "ShotgunAmmo":
                player.GetComponent<Inventory>().ammunition[Ammunition.shotgunAmmo] += 5;
                return true;
            case "LaserAmmo":
                player.GetComponent<Inventory>().ammunition[Ammunition.laserBattery] += 10;
                return true;
            case "TeleportFuel":
                player.GetComponent<Inventory>().ammunition[Ammunition.teleportationFuel] = 1;
                return true;
            case "Armor":
                armorController.setPickUpArmor(true);
                return true;
            default:
                Debug.LogWarning($"WARNING: No handler implemented for tag {obj.tag}.");
                return false;
        }
    }

    public void ResetPlayer()
    {
        healthController.redSplatterImg.enabled = false;
        healthController.redSplatterImg.enabled = true;
        healthController.currentPlayerHP = healthController.maxPlayerHP;
        healthController.playerLives -= 1;
        healthController.UpdateHealth();
        if (healthController.playerLives < 0)
        {
            
            start_text.gameObject.SetActive(false);
            dead_text.gameObject.SetActive(true);
            anim.SetTrigger("transition");
               
            //foreach (Transform child in transform)
            //{
            //    child.gameObject.SetActive(false);
            //}
            transform.GetComponent<PlayerMovement>().enabled = false;
            DisableWeapons(transform);
            DisableCameraMovement(transform);

            StartCoroutine(ReadingPause());

            //Cursor.lockState = CursorLockMode.None;
            //SceneManager.LoadScene(0);

            
        }
        else
        {
            player.transform.position = respawnPoint.transform.position;
        }
        
    }

    private IEnumerator ReadingPause()
    {
        pauseForReading = true;
        
        yield return new WaitForSeconds(secondsOfWaiting);

        pauseForReading = false;

        
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(0);
        
    }
    private void DisableWeapons(Transform t)
    {
        foreach (Transform child in t)
        {
            var a = child.GetComponent<WeaponSwitching>();
            if (a != null)
            {
                a.enabled = false;
            }
            else
            {
                DisableWeapons(child);
            }
        }
    }

    private void DisableCameraMovement(Transform t)
    {
        foreach (Transform child in t)
        {
            var a = child.GetComponent<MouseLook>();
            if (a != null)
            {
                a.enabled = false;
            }
            else
            {
                DisableCameraMovement(child);
            }
        }
    }

}

